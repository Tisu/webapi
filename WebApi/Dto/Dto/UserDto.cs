﻿using Microsoft.Build.Framework;
using Newtonsoft.Json;

namespace Dto
{
    public sealed class UserDto
    {
        [Required]
        [JsonProperty(PropertyName = "UserName")]
        public string m_user_name { get; set; }
        [Required]
        [JsonProperty(PropertyName = "Password")]
        public string m_password { get; set; }
        [JsonProperty(PropertyName = "FirstName")]
        public string m_first_name { get; set; }
        [JsonProperty(PropertyName = "LastName")]
        public string m_last_name { get; set; }
        [JsonProperty(PropertyName = "FullName")]
        public string m_full_name => $"{m_first_name} {m_last_name}";
    }
}