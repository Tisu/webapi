﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Dto
{
    public class GameScoreDto
    {
        [JsonProperty(PropertyName = "Id")]
        public int m_id { get; set; }
        [JsonProperty(PropertyName = "PlayerName")]
        public string m_player_name { get; set; }
        [JsonProperty(PropertyName = "Score")]
        public uint m_score { get; set; }
        [JsonProperty(PropertyName = "GameLevel")]
        public int m_game_level { get; set; }
        [JsonProperty(PropertyName = "CreationTime")]
        public string m_creation_time;
    }
}
