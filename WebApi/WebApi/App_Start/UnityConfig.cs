using Microsoft.Practices.Unity;
using System.Web.Http;
using Database;
using Unity.WebApi;
using WebApi.Controllers;

namespace WebApi
{
    public static class UnityConfig
    {
        private static DatabaseMock m_database = new DatabaseMock();
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterInstance(typeof(IDatabaseMock), m_database);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}