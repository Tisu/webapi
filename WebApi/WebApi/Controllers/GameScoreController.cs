﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Database;
using Dto;

namespace WebApi.Controllers
{
    [RoutePrefix("api/GameScore")]
    [Authorize]
    public class GameScoreController : ApiController
    {
        private IDatabaseMock m_database { get; set; }

        public GameScoreController(IDatabaseMock database)
        {
            m_database = database;
        }
        [AllowAnonymous]
        [HttpGet, Route("")]
        [ResponseType(typeof(IEnumerable<GameScoreDto>))]
        public IHttpActionResult Get()
        {
            return Ok(m_database.m_database.Values);
        }

        [AllowAnonymous]
        [HttpGet, Route("{id}", Name = "GetGameScore")]
        [ResponseType(typeof(GameScoreDto))]
        public IHttpActionResult Get(int id)
        {
            GameScoreDto result;
            if(m_database.m_database.TryGetValue(id, out result))
            {
                return Ok(result);
            }
            return NotFound();
        }

        [HttpPost, Route("")]
        [ResponseType(typeof(GameScoreDto))]
        public IHttpActionResult Post([FromBody]GameScoreDto game_score)
        {
            var id = m_database.m_database.Count > 0 ? m_database.m_database.Keys.Max() + 1 : 0;
            game_score.m_id = id;
            game_score.m_creation_time = DateTime.UtcNow.ToString();
            if (!m_database.m_database.TryAdd(id, game_score))
            {
                return Conflict();
            }

            return CreatedAtRoute("GetGameScore", new { id = id }, game_score);
        }

        [HttpPut, Route("{id}")]
        [ResponseType(typeof(GameScoreDto))]
        public IHttpActionResult Put(int id, [FromBody]GameScoreDto game_score)
        {
            GameScoreDto game_score_db;
            if (!m_database.m_database.TryGetValue(id,out game_score_db))
            {
                return NotFound();
            }
            game_score_db.m_id = game_score.m_id;
            game_score_db.m_game_level = game_score.m_game_level;
            game_score_db.m_player_name = game_score.m_player_name;
            game_score_db.m_score = game_score.m_score;
            game_score_db.m_creation_time = DateTime.UtcNow.ToString();

            return Ok();
        }

        [HttpDelete, Route("{id}")]
        [ResponseType(typeof(GameScoreDto))]
        public IHttpActionResult Delete(int id)
        {
            if (!m_database.m_database.ContainsKey(id))
            {
                return NotFound();
            }
            GameScoreDto game_score;
            if (!m_database.m_database.TryRemove(id, out game_score))
            {
                return Conflict();
            }
            return Ok();
        }
    }
}
