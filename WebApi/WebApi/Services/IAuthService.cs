﻿using Dto;

namespace WebApi.Services
{
    public interface IAuthService
    {
        bool AuthenticateUser(string userName, string password, out UserDto user);
    }
}