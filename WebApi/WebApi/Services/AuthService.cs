﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Dto;

namespace WebApi.Services
{
    public sealed class AuthService : IAuthService
    {
        private static readonly ConcurrentDictionary<string, UserDto> m_users =
            new ConcurrentDictionary<string, UserDto>
            {
                ["lkurzyniec"] = new UserDto { m_first_name = "Łukasz", m_last_name = "Kurzyniec", m_user_name = "lkurzyniec", m_password = "wieza123" },
                ["szpi"] = new UserDto { m_first_name = "Piotr", m_last_name = "Szkudlarski", m_user_name = "szpi", m_password = "copypasterino" },
            };
        public bool AuthenticateUser(string userName, string password, out UserDto user)
        {
            if(!m_users.TryGetValue(userName, out user))
            {
                return false;
            }

            return user.m_password == password;
        }
    }
}