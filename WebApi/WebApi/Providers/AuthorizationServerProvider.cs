﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Dto;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using WebApi.Services;

namespace WebApi.Providers
{
    public sealed class AuthorizationServerProvider: OAuthAuthorizationServerProvider
    {
        private readonly IAuthService m_auth_service;

        public AuthorizationServerProvider(IAuthService auth_service)
        {
            m_auth_service = auth_service;
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
           
            UserDto user;
            if(!m_auth_service.AuthenticateUser(context.UserName, context.Password, out user))
            {
                context.SetError("invalid_grant", "user name or password is incorrect");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {"fullname", user.m_full_name}
            });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
        }

        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach(var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
        }
    }
}