﻿using System;

namespace WebApi.Providers
{
    public sealed class OAuthConfiguration
    {
        public static readonly TimeSpan TOKEN_EXPIRE_TIME = TimeSpan.FromDays(1);
    }
}