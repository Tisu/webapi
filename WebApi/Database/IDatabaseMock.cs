﻿using System.Collections.Concurrent;
using Dto;

namespace Database
{
    public interface IDatabaseMock
    {
        ConcurrentDictionary<int, GameScoreDto> m_database { get; set; }
    }
}