using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;

namespace Database
{
    public class DatabaseMock : IDatabaseMock
    {
        public ConcurrentDictionary<int, GameScoreDto> m_database { get; set; } = new ConcurrentDictionary<int, GameScoreDto>()
        {
            [1] = new GameScoreDto() { m_id = 1, m_game_level = 1, m_player_name = "Tisu", m_score = uint.MaxValue },
            [2] = new GameScoreDto() { m_id = 2, m_player_name = "Pendzel", m_game_level = 1, m_score = 0 }
        };
    }
}
